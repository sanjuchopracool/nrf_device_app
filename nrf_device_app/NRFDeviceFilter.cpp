#include "NRFDeviceFilter.h"

NRFDeviceFilter::NRFDeviceFilter(quint16 invid,
                                 quint16 inpid,
                                 QString inVendorString,
                                 QString inDeviceRegEx)
    : vid(invid),
      pid(inpid),
      vendorString(inVendorString),
      devicePattern(inDeviceRegEx)
{

}
