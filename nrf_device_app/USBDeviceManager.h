#ifndef USBDEVICEMANAGER_H
#define USBDEVICEMANAGER_H

#include <libusb-1.0/libusb.h>

#include "NRFDeviceFilter.h"
#include <QList>

struct NRFDevice
{
    NRFDevice( const QString& inDeviceName, libusb_device* inDevicePtr)
        : deviceName(inDeviceName),
          devicePtr(inDevicePtr)
    {}

    QString     deviceName;
    libusb_device  *devicePtr = nullptr;
};

class USBDeviceManager
{
public:
    USBDeviceManager( const NRFDeviceFilter &inFilter);
    ~USBDeviceManager();

    void resetFilter( const NRFDeviceFilter &inFilter);
    const NRFDeviceFilter &getFilter() const;

    const QList<NRFDevice>& filteredDevices() const;

private:
    libusb_context  *libusbContext;
    NRFDeviceFilter nrfDeviceFilter;

    QList<NRFDevice>    devices;
private:
    void updateDevices();
};

#endif // USBDEVICEMANAGER_H
