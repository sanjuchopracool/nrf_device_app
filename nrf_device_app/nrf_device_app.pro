#-------------------------------------------------
#
# Project created by QtCreator 2015-07-28T13:55:22
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = nrf_device_app
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    NRFDeviceFilter.cpp \
    USBDeviceManager.cpp

HEADERS += \
    NRFDeviceFilter.h \
    USBDeviceManager.h

LIBS += -lusb-1.0
