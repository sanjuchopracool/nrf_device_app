#include "USBDeviceManager.h"
#include <QDebug>

USBDeviceManager::USBDeviceManager(const NRFDeviceFilter &inFilter)
    : nrfDeviceFilter(inFilter)
{
    if ( libusb_init( &libusbContext) < 0 )
    {
        libusb_exit(libusbContext);
        qCritical("LibUsb Init Error %d",libusbContext);
        return;
        // TODO throw exception
    }

    updateDevices();
}

USBDeviceManager::~USBDeviceManager()
{
    libusb_exit(libusbContext);
}

void USBDeviceManager::resetFilter(const NRFDeviceFilter &inFilter)
{

}

const NRFDeviceFilter &USBDeviceManager::getFilter() const
{

}

const QList<NRFDevice> &USBDeviceManager::filteredDevices() const
{
    return devices;
}

void USBDeviceManager::updateDevices()
{
    devices.clear();
    ssize_t cnt;  // holding number of devices in list
    libusb_device** devs;
    cnt = libusb_get_device_list(libusbContext, &devs);  // get the list of devices

    if (cnt > 0)
    {
        for (int i = 0; i < cnt; i++)
        {
            libusb_device* dev = devs[i];
            libusb_device_descriptor desc;

            if (libusb_get_device_descriptor(dev, &desc) == 0)
            {
                if(nrfDeviceFilter.vid == desc.idVendor &&
                        nrfDeviceFilter.pid == desc.idProduct )
                {
                    // check for the manufacturer string and device strings
                    libusb_device_handle* devHandle = NULL;
                    int result = libusb_open(dev, &devHandle);
                    if(0 == result)
                    {
                        unsigned char stringBuffer[512];
                        libusb_get_string_descriptor_ascii(devHandle, desc.iManufacturer,stringBuffer,512);

                        QString manufacturer = QString::fromLocal8Bit((char*)stringBuffer);
                        if( manufacturer == nrfDeviceFilter.vendorString )
                        {
                            libusb_get_string_descriptor_ascii(devHandle, desc.iProduct, stringBuffer,512);
                            QString deviceName = QString::fromLocal8Bit((char*)stringBuffer);
                            if( deviceName.contains(nrfDeviceFilter.devicePattern) )
                            {
                                //TODO log?
                                qDebug() << "Found device: " << manufacturer << deviceName;
                                devices.append(NRFDevice(deviceName, dev));
                            }
                        }
                        libusb_close(devHandle);
                    }
                }
            }
        }
    }
    libusb_free_device_list(devs, 1);
}
