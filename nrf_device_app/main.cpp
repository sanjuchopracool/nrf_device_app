#include <iostream>
#include "USBDeviceManager.h"
using namespace std;

#define USB_LED_OFF 0
#define USB_LED_ON  1
#define GET_REGISTER_VALUE  2

int main( int argc, char** argv)
{
    if( argc == 2 )
    {

        USBDeviceManager devManager(NRFDeviceFilter(0x16C0,0x05DC, "KUMAR", "NRFUSBDEVICE"));
        if( devManager.filteredDevices().count() )
        {
            libusb_device* dev = devManager.filteredDevices().first().devicePtr;
            libusb_device_handle* devHandle = NULL;
            int result = libusb_open(dev, &devHandle);
            unsigned char buffer[512];
            std::string command = std::string(argv[1]);
            if(0 == result)
            {
                if( command == "off" )
                {
                    libusb_control_transfer(devHandle,
                                            LIBUSB_REQUEST_TYPE_VENDOR  | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
                                            USB_LED_OFF,
                                            0,
                                            0,
                                            buffer,
                                            sizeof(buffer),
                                            5000);
                }
                else if ( command == "on" )
                {
                    libusb_control_transfer(devHandle,
                                            LIBUSB_REQUEST_TYPE_VENDOR  | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
                                            USB_LED_ON,
                                            0,
                                            0,
                                            buffer,
                                            sizeof(buffer),
                                            5000);
                }
                else if (command == "readRegister")
                {
                    for ( uint16_t i = 0; i <= 0x1D; ++i)
                    {
                        int nBytes =  libusb_control_transfer(devHandle,
                                                              LIBUSB_REQUEST_TYPE_VENDOR  | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
                                                              GET_REGISTER_VALUE,
                                                              i,
                                                              0,
                                                              buffer,
                                                              sizeof(buffer),
                                                              5000);
                        printf("Register %02X: %02X\n", i, buffer[0]);
                    }
                }
                libusb_close(devHandle);
            }
        }
    }
    return 0;
}
