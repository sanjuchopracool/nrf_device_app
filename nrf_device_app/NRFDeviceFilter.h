#ifndef NRFDEVICEFILTER_H
#define NRFDEVICEFILTER_H

#include <QString>

// It have vid , pid , vendor string and a regex for device

struct NRFDeviceFilter
{
public:
    NRFDeviceFilter( quint16 invid,
                     quint16 inpid,
                     QString inVendorString,
                     QString inDeviceRegEx);

    quint16     vid;
    quint16     pid;
    QString     vendorString;
    QString     devicePattern;
};

#endif // NRFDEVICEFILTER_H
